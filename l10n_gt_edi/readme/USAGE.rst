**Nota** Es importante mencionar que este módulo sólo se encarga de la generación del XML y prepara el
entorno para la salida y entrada de datos; se debe instalar algún otro módulo que se enecargue de la
comunicación hacía un servicio de terceros para el timbrado y recepción del estado del mismo.
